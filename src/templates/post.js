import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout>
      <article
        id="post-1"
        className="post-1 post type-post status-publish format-standard hentry category-uncategorised"
      >
        <header>
          <h1>{frontmatter.title}</h1>
          <div className="entry-meta">
            <span className="posted-on">
              Posted on{" "}
              <a href="" rel="bookmark">
                <time
                  className="entry-date published updated"
                  datetime="2020-05-16T14:39:24+01:00"
                >
                  {frontmatter.date}
                </time>
              </a>
            </span>
            <span className="byline">
              {" "}
              by{" "}
              <span className="author vcard">
                <a className="url fn n" href="">
                  {frontmatter.author}
                </a>
              </span>
            </span>{" "}
          </div>
        </header>

        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: html }}
        />

        <footer>
          <span className="cat-links">
            Posted in{" "}
            <a
              href="http://localhost:8888/susty-wp/category/uncategorised/"
              rel="category tag"
            >
              {frontmatter.category}
            </a>
          </span>
        </footer>
      </article>
      <div id="comments" className="comments-area">
        <h2 className="comments-title">
          One thought on “<span>Hello world!</span>”{" "}
        </h2>

        <ol className="comment-list">
          <li id="comment-1" className="comment even thread-even depth-1">
            <article id="div-comment-1" className="comment-body">
              <footer className="comment-meta">
                <div className="comment-author vcard">
                  <img
                    alt=""
                    src="http://1.gravatar.com/avatar/d7a973c7dab26985da5f961be7b74480?s=32&amp;d=mm&amp;r=g"
                    srcset="http://1.gravatar.com/avatar/d7a973c7dab26985da5f961be7b74480?s=64&amp;d=mm&amp;r=g 2x"
                    className="avatar avatar-32 photo"
                    height="32"
                    width="32"
                  />{" "}
                  <b className="fn">
                    <a
                      href="https://wordpress.org/"
                      rel="external nofollow ugc"
                      className="url"
                    >
                      A WordPress Commenter
                    </a>
                  </b>{" "}
                  <span className="says">says:</span>{" "}
                </div>

                <div className="comment-metadata">
                  <a href="">
                    <time datetime="2020-05-16T14:39:24+01:00">
                      16th May 2020 at 2:39 pm{" "}
                    </time>
                  </a>
                  <span className="edit-link">
                    <a className="comment-edit-link" href="">
                      Edit
                    </a>
                  </span>{" "}
                </div>
              </footer>
              <div className="comment-content">
                <p>
                  Hi, this is a comment.
                  <br />
                  To get started with moderating, editing, and deleting
                  comments, please visit the Comments screen in the dashboard.
                  <br />
                  Commenter avatars come from{" "}
                  <a href="https://gravatar.com">Gravatar</a>.
                </p>
              </div>
              <div className="reply">
                <a
                  rel="nofollow"
                  className="comment-reply-link"
                  href=""
                  data-commentid="1"
                  data-postid="1"
                  data-belowelement="div-comment-1"
                  data-respondelement="respond"
                  aria-label="Reply to A WordPress Commenter"
                >
                  Reply
                </a>
              </div>{" "}
            </article>
          </li>
        </ol>

        <div id="respond" className="comment-respond">
          <h3 id="reply-title" className="comment-reply-title">
            Leave a Reply{" "}
            <small>
              <a
                rel="nofollow"
                id="cancel-comment-reply-link"
                href="/susty-wp/2020/05/16/hello-world/#respond"
              >
                Cancel reply
              </a>
            </small>
          </h3>
        </div>
      </div>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        slug
        title
        author
        category
      }
    }
  }
`;

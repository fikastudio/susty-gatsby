import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import Image from "../components/image";
import SEO from "../components/seo";
import PostLink from "../components/post-link";

const IndexPage = ({
  data: {
    allMarkdownRemark: { edges },
  },
}) => {
  console.log(edges);
  const Posts = edges
    .filter((edge) => !!edge.node.frontmatter.date) // You can filter your posts based on some criteria
    .map((edge) => (
      <article
        id="post-1"
        className="post-1 post type-post status-publish format-standard hentry category-uncategorised"
      >
        <header>
          <h2>
            <PostLink key={edge.node.id} post={edge.node} />
          </h2>{" "}
          <div className="entry-meta">
            <span className="posted-on">
              Posted on{" "}
              <a
                href="http://localhost:8888/susty-wp/2020/05/16/hello-world/"
                rel="bookmark"
              >
                <time
                  className="entry-date published updated"
                  datetime="2020-05-16T14:39:24+01:00"
                >
                  {edge.node.frontmatter.date}
                </time>
              </a>
            </span>
            <span className="byline">
              {" "}
              by{" "}
              <span className="author vcard">
                <a
                  className="url fn n"
                  href="http://localhost:8888/susty-wp/author/drydendev/"
                >
                  {edge.node.frontmatter.author}
                </a>
              </span>
            </span>{" "}
          </div>
        </header>

        <div className="blog-post-content">
          <p>{edge.node.excerpt} </p>
        </div>

        <footer>
          <span className="cat-links">
            Posted in{" "}
            <a
              href="http://localhost:8888/susty-wp/category/uncategorised/"
              rel="category tag"
            >
              {edge.node.frontmatter.category}
            </a>
          </span>
          <span className="comments-link">
            <a href="http://localhost:8888/susty-wp/2020/05/16/hello-world/#comments">
              1 Comment
              <span className="screen-reader-text"> on Hello world!</span>
            </a>
          </span>
        </footer>
      </article>
    ));

  return (
    <Layout>
      <SEO title="Home" />
      <div>{Posts}</div>
    </Layout>
  );
};

export default IndexPage;

export const pageQuery = graphql`
  query {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          excerpt
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            slug
            title
            author
            category
          }
        }
      }
    }
  }
`;

// Gatsby supports TypeScript natively!
import React from "react";
import { PageProps, Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";

const SamplePage = (props: PageProps) => (
  <Layout>
    <SEO title="Page two" />
    <article
      id="post-2"
      className="post-2 page type-page status-publish hentry"
    >
      <header>
        <h1>Sample Page</h1>{" "}
      </header>

      <div>
        <p>
          This is an example page. It’s different from a blog post because it
          will stay in one place and will show up in your site navigation (in
          most themes). Most people start with an About page that introduces
          them to potential site visitors. It might say something like this:
        </p>

        <blockquote className="wp-block-quote">
          <p>
            Hi there! I’m a bike messenger by day, aspiring actor by night, and
            this is my website. I live in Los Angeles, have a great dog named
            Jack, and I like piña coladas. (And gettin’ caught in the rain.)
          </p>
        </blockquote>

        <p>…or something like this:</p>

        <blockquote className="wp-block-quote">
          <p>
            The XYZ Doohickey Company was founded in 1971, and has been
            providing quality doohickeys to the public ever since. Located in
            Gotham City, XYZ employs over 2,000 people and does all kinds of
            awesome things for the Gotham community.
          </p>
        </blockquote>

        <p>
          As a new WordPress user, you should go to{" "}
          <a href="http://localhost:8888/susty-wp/wp-admin/">your dashboard</a>{" "}
          to delete this page and create new pages for your content. Have fun!
        </p>
      </div>

      <footer>
        <span className="edit-link">
          <a
            className="post-edit-link"
            href="http://localhost:8888/susty-wp/wp-admin/post.php?post=2&amp;action=edit"
          >
            Edit <span className="screen-reader-text">Sample Page</span>
          </a>
        </span>{" "}
      </footer>
    </article>
  </Layout>
);

export default SamplePage;

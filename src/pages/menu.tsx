// Gatsby supports TypeScript natively!
import React from "react";
import { PageProps, Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";

const Menu = (props: PageProps) => (
  <Layout>
    <SEO title="Menu" />
    <nav id="site-navigation" className="main-navigation">
      <h1>Menu</h1>
      <div className="menu-menu-1-container">
        <ul id="primary-menu" className="menu">
          <li
            id="menu-item-9"
            className="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-9"
          >
            <Link to="/">Home</Link>
          </li>
          <li
            id="menu-item-15"
            className="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"
          >
            <Link to="/sample-page">Sample Page</Link>
          </li>
        </ul>
      </div>{" "}
    </nav>
  </Layout>
);

export default Menu;

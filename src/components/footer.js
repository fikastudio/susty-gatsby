import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";

const Footer = () => (
  <footer id="colophon">
    <a href="https://wordpress.org">Proudly powered by Gatsby </a>
    <span> | </span>
    Theme based on: <a href="https://github.com/jacklenox/susty">
      Susty
    </a> by <a href="https://blog.jacklenox.com">Jack&nbsp;Lenox</a>. Developed
    by <a href="https://www.fika.studio/">Fika Studio</a>
  </footer>
);

Footer.propTypes = {
  siteTitle: PropTypes.string,
};

Footer.defaultProps = {
  siteTitle: ``,
};

export default Footer;

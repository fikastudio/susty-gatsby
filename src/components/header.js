import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";

const Header = ({ siteTitle }) => (
  <header id="masthead">
    <div className="logo">
      <Link to="/">
        <img
          alt="Susty WP logo"
          src="http://localhost:8888/susty-wp/wp-content/themes/susty-master/images/eco-chat.svg"
        />
        <span className="screen-reader-text">Home</span>
      </Link>
    </div>
    <h1>
      <Link to="/">Susty Gatsby</Link>
    </h1>
    <Link to="/menu">Menu</Link>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;

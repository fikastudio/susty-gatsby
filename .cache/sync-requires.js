const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/src/pages/404.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/src/pages/index.js"))),
  "component---src-pages-menu-tsx": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/src/pages/menu.tsx"))),
  "component---src-pages-sample-page-tsx": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/src/pages/sample-page.tsx"))),
  "component---src-templates-post-js": hot(preferDefault(require("/Users/drydenwilliams/projects/susty-gatsby/src/templates/post.js")))
}

